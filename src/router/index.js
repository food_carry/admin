import { createRouter, createWebHistory } from "vue-router";
import OrdersView from "../views/OrdersView.vue";
import MenuView from "@/views/MenuView.vue";
import StopListView from "@/views/StopListView.vue";
//import NotificationView from "@/views/NotificationsView";
//import SettingsView from "@/views/SettingsView";
import LoginView from "@/views/LoginView.vue";
import ScoreboardView from "@/views/ScoreboardView.vue";

import store from "@/store";

const routes = [
  {
    path: "/orders",
    name: "orders",
    component: OrdersView,
    meta: { requiresAuth: true },
  },
  {
    path: "/menu",
    name: "menu",
    component: MenuView,
    meta: { requiresAuth: true },
  },
  {
    path: "/stop-list",
    name: "stop-list",
    component: StopListView,
    meta: { requiresAuth: true },
  },
  {
    path: "/scoreboard",
    name: "scoreboard",
    component: ScoreboardView,
    meta: { requiresAuth: true },
  },
  /*{
    path: "/settings",
    name: "settings",
    component: SettingsView,
    meta: { requiresAuth: true },
  },*/
  /*  {
    path: "/notifications",
    name: "notifications",
    component: NotificationView,
    meta: { requiresAuth: true },
  },*/
  {
    path: "/login",
    name: "login",
    component: LoginView,
    meta: { requiresAuth: false },
  },
  {
    path: "/",
    redirect: "/orders",
    meta: { requiresAuth: true },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to) => {
  if (to.meta.requiresAuth && !store.getters.IS_LOGGED_IN) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
