import api from "@/api/index";
import store from "@/store";


// Если токен не действителен, переход в login
export default function setup() {
  api.interceptors.response.use(undefined, function (err) {
    return new Promise(function () {
      if (
        err.response.status === 401 &&
        err.config &&
        !err.config.__isRetryRequest
      ) {
        store.commit("DELETE_TOKEN");
        store.commit("SET_LOGOUT_STATUS");
      }
      throw err;
    });
  });
}
