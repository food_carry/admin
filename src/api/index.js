import axios from "axios";

let headers = {
  Accept: "application/json",
};
const token = localStorage.getItem("token");
if (token) {
  headers["Authorization"] = `Token ${token}`;
}

export default axios.create({
  baseURL: "https://www.foodcarryapi.m1studios.ru/",
  timeout: 1000,
  headers,
});
