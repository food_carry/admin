import { createStore } from "vuex";
import dishes from "@/store/modules/dishes.js";
import orders from "@/store/modules/orders.js";
import user from "@/store/modules/user.js";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: { dishes, orders, user },
});
