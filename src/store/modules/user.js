import api from "@/api";

const state = {
  token: localStorage.getItem("token") || "",
  login_status: "",
  user: null,
  catering: {},
};
const getters = {
  IS_LOGGED_IN: (state) => !!state.token,
  GET_TOKEN: (state) => state.token,
  GET_LOGIN_STATUS: (state) => state.login_status,
};
const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },
  // Изменение статуса
  SET_LOADING_STATUS(state) {
    state.login_status = "loading";
  },
  SET_SUCCESS_STATUS(state, token) {
    state.login_status = "success";
    state.token = token;
  },
  SET_ERROR_STATUS(state) {
    state.login_status = "error";
  },
  SET_LOGOUT_STATUS(state) {
    state.login_status = "logout";
    state.user = null;
  },
  DELETE_TOKEN(state) {
    delete api.defaults.headers.common["Authorization"];
    localStorage.removeItem("token");
    state.token = "";
  },
  SET_CATERING_INFO(state, catering) {
    state.catering = catering;
  },
};
const actions = {
  async log_in({ commit }, user) {
    commit("SET_LOADING_STATUS");
    commit("DELETE_TOKEN");

    return await api
      .post("auth/token/login/", user)
      .then((response) => {
        const token = response.data.auth_token;

        localStorage.setItem("token", token);
        api.defaults.headers.common["Authorization"] = `Token ${token}`;
        commit("SET_SUCCESS_STATUS", token);
      })
      .catch((err) => {
        console.log(err);
        commit("DELETE_TOKEN");
        commit("SET_ERROR_STATUS");
        return err;
      });
  },
  // Получить информацию о пользователе
  async get_user({ commit }) {
    api.get("api/v1/auth/users/me/").then((response) => {
      commit("SET_USER", response.data);
    });
  },
  async log_out({ commit }) {
    api.post("auth/token/logout").then(() => {
      commit("DELETE_TOKEN");
      commit("SET_LOGOUT_STATUS");
    });
  },
  async get_catering_info({ commit }) {
    // TODO: Получить заведение по id пользователя
    api
      .get("api/v1/catering/1/")
      .then((response) => commit("SET_CATERING_INFO", response.data))
      .catch((err) => console.log(err));
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
