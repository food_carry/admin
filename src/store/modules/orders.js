//import api from "@/api";

import api from "@/api";

const state = {
  orders: [],
  paid_types: {
    cash: "Наличными",
    card: "Картой",
  },
  last_update: null, // Время, когда было получено последнее обновление списка заказов
  status_list: [
    {
      name: "",
      title: "Все",
      bg_color: "#ff4200",
      text_color: "#ffffff",
    },
    { name: "new", title: "Новый", bg_color: "#FFE8B2", text_color: "#E3A917" },
    {
      name: "in_process",
      title: "Готовится",
      bg_color: "#DBDFF8",
      text_color: "#758DFB",
    },
    {
      name: "ready",
      title: "Готов",
      bg_color: "#CFE8E4",
      text_color: "#3DB39D",
    },
    {
      name: "cancel",
      title: "Отменен",
      bg_color: "#FFE8E0",
      text_color: "#FD571D",
    },
    {
      name: "closed",
      title: "Закрыт",
      bg_color: "#e5e5e5",
      text_color: "#333232",
    },
  ],
};

const getters = {
  // TODO: Сделать сортировку по последнему обновлению заказа
  // TODO: Убрать лишние геттеры во всем проекте
  // Получение заказов по статусу
  GET_ORDERS_BY_STATUSES: (state) => {
    const orders = {};
    state.status_list.forEach((status) => (orders[status.name] = []));
    state.orders.forEach((order) => {
      orders[order.status].push(order);
    });
    return orders;
  },
  GET_PAID_TYPES: (state) => state.paid_types,
  GET_STATUS_LIST: (state) => state.status_list,
  GET_LAST_UPDATE: (state) => state.last_update,
};
//TODO: Убрать повторение
const mutations = {
  SET_ORDERS: (state, orders) => {
    // Разворот массива, для отображения новых вверху
    state.orders = orders.reverse();
  },
  ADD_ORDER: (state, order) => {
    state.orders.push(order);
  },
  EDIT_ORDER_BY_ID: (state, order) => {
    const orderIdx = state.orders.map((order) => order.id).indexOf(order.id);
    state.orders[orderIdx] = order;
  },
  SET_LAST_UPDATE: (state) => {
    const maxTimestamp = Math.max(
      ...state.orders.map((order) => +new Date(order.update_time))
    );
    const timeIdx = state.orders
      .map((order) => +new Date(order.update_time))
      .indexOf(maxTimestamp);
    state.last_update = state.orders[timeIdx].update_time;
  },
  EDIT_ORDER_STATUS: (state, { order_id, status }) => {
    state.orders.find((order) => order.id === order_id).status = status;
  },
  DELETE_ORDER: (state, order_id) => {
    const order_idx = state.orders.map((order) => order.id).indexOf(order_id);
    state.orders.splice(order_idx, 1);
  },
};

const actions = {
  // TODO: Сделать поддержку нескольких заведений
  // Получить блюда в заведении (last_update - время, начиная с которого, нужно получить обновления)
  get_orders: async ({ commit, dispatch, state }) => {
    const last_update = state.last_update;
    const params = {};
    if (last_update) params.last_update = last_update;
    api
      .get("api/v1/catering/1/orders/", { params })
      .then((response) => {
        if (!state.orders.length) {
          // Если список заказов пустой, то добавить все
          commit("SET_ORDERS", response.data);
        } else {
          // Если список не пустой, то добавить или изменить
          response.data.forEach((order) => dispatch("add_or_edit_dish", order));
        }
        commit("SET_LAST_UPDATE");
      })
      .catch((err) => {
        console.log(err);
      });
  },

  // Добавить блюдо или изменить, если уже есть в списке
  add_or_edit_dish: ({ commit }, order) => {
    const orderIds = state.orders.map((order) => order.id);
    // TODO: При изменении статуса, 2 раз обновляется информация
    if (orderIds.indexOf(order.id) !== -1) {
      commit("EDIT_ORDER_BY_ID", order);
    } else {
      commit("ADD_ORDER", order);
    }
  },
  // Изменить статус заказа
  edit_order_status({ commit }, { order_id, status }) {
    api
      .patch(`api/v1/order/${order_id}/`, { status })
      .then((response) =>
        commit("EDIT_ORDER_STATUS", {
          order_id: response.data.id,
          status: response.data.status,
        })
      )
      .catch((err) => {
        console.log(err);
      });
  },
  // Удалить заказ
  delete_order({ commit }, order_id) {
    api
      .delete(`api/v1/order/${order_id}/`)
      .then(() => commit("DELETE_ORDER", order_id))
      .catch((err) => {
        console.log(err);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
