import api from "@/api";
//TODO: Добавить обработку ошибок
const state = {
  categories: [],
  dishes: [],
  stop_list: [],
};
// Добавить
const getters = {
  // Получить категории блюда
  GET_CATEGORIES_FOR_DISH: (state) => (dish) => {
    return state.categories.filter((category) =>
      dish.categories?.includes(category.id)
    );
  },
  GET_DISHES_BY_CATEGORIES: (state) => {
    const dishes = {};
    state.categories.forEach((category) => (dishes[category.name] = []));
    state.dishes.forEach((dish) => {
      dish.categories.forEach((category_id) => {
        const category_name = state.categories.find(
          (category) => category.id === category_id
        ).name;
        dishes[category_name].push(dish);
      });
    });
    return dishes;
  },
  // TODO: Переделать без параметра
  GET_DISHES_BY_ID: (state) => (dish_id) =>
    state.dishes.find((dish) => dish_id === dish.id),
  GET_DEFAULT_DISH: () => {
    return {
      categories: [],
      catering: 1, // TODO: Добавить поддержку нескольких заведений
      desc: "Описание",
      name: "Блюдо",
      price: 100,
    };
  },
};
const mutations = {
  SET_CATEGORIES(state, categories) {
    state.categories = categories;
    state.categories.unshift({
      name: "",
      title: "Все",
      bg_color: "#ff4200",
      text_color: "#ffffff",
    }); // Опция для всех категорий
  },
  SET_DISHES(state, dishes) {
    state.dishes = dishes;
  },
  SET_DISH_DATA_BY_ID(state, { dish_id, dish }) {
    const dish_idx = state.dishes.map((dish) => dish.id).indexOf(dish_id);
    state.dishes[dish_idx] = dish;
  },
  RESET_DISHES(state) {
    state.dishes = [];
  },
  ADD_DISH(state, dish) {
    state.dishes.push(dish);
  },
  DELETE_DISH_BY_ID(state, dish_id) {
    const dish_idx = state.dishes.map((dish) => dish.id).indexOf(dish_id);
    state.dishes.splice(dish_idx, 1);
  },
  TOGGLE_DISH_STOP_LIST(state, { dish_id, is_stopped }) {
    const dish_idx = state.dishes.map((dish) => dish.id).indexOf(dish_id);
    state.dishes[dish_idx].is_stopped = is_stopped;
  },
};
const actions = {
  // Получить категории
  get_categories: async ({ commit }) => {
    api
      .get("api/v1/category/")
      .then((response) => commit("SET_CATEGORIES", response.data))
      .catch((err) => {
        console.log(err);
      });
  },
  // Получить блюда по категории
  get_dishes: async ({ commit }, category = "") => {
    // TODO: Добавить поддержку разных заведений
    const params = {};
    if (category !== "") params["categories[]"] = category;
    api
      .get(`api/v1/catering/1/dishes/`, {
        params,
      })
      .then((response) => commit("SET_DISHES", response.data));
  },
  // Сбросить список блюд
  reset_dishes: ({ commit }) => {
    commit("RESET_DISHES");
  },
  // Добавить блюдо
  add_dish({ commit }, dish) {
    return api.post("api/v1/dish/", dish).then((response) => {
      commit("ADD_DISH", response.data);
      return response;
    });
  },
  // Редактировать информацию о блюде по id
  set_dish_data_by_id({ commit }, { dish, dish_id }) {
    api
      .patch(`api/v1/dish/${dish_id}/`, dish)
      .then((response) => {
        commit("SET_DISH_DATA_BY_ID", { dish: response.data, dish_id });
      })
      .catch((err) => {
        console.log(err);
      });
  },
  set_dish_img_by_id({ commit }, { dish_img, dish_id }) {
    api
      .patch(
        `api/v1/dish/${dish_id}/`,
        { img: dish_img },
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((response) => {
        commit("SET_DISH_DATA_BY_ID", { dish: response.data, dish_id });
      });
  },
  // Добавить блюдо в стоп-лист или убрать оттуда
  toggle_dish_stop_list({ commit }, { dish_id, is_stopped }) {
    api.patch(`api/v1/dish/${dish_id}/`, { is_stopped }).then(() => {
      commit("TOGGLE_DISH_STOP_LIST", {
        dish_id,
        is_stopped,
      });
    });
  },
  // Удалить блюдо
  delete_dish({ commit }, dish_id) {
    api
      .delete(`api/v1/dish/${dish_id}/`)
      .then(() => commit("DELETE_DISH_BY_ID", dish_id));
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
